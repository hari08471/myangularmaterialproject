import { ThrowStmt } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';
import { DataService } from './data.service';
import { Product } from './Product.model';
import  { Router } from '@angular/router'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  
  constructor(private router: Router) {

  }
  ngOnInit(){    
    

  }

  newProduct(){
   this.router.navigate(['/Product']);
  }

  editProduct(){
    this.router.navigate(['/Product-list']);
   }

}
