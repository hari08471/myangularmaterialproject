import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-products-list',
  templateUrl: './products-list.component.html',
  styleUrls: ['./products-list.component.css']
})
export class ProductsListComponent implements OnInit {

  constructor() { }

  form!: FormGroup;
  ngOnInit() {
    this.form = new FormGroup({
      patient_name: new FormControl(''),
      patient_age: new FormControl(''),
    });
  }
  onClickSubmit(formData: { patient_name: string; patient_age: string; }) {
    console.log('Coronal Effected Patient name is : ' + formData.patient_name);
    console.log('Coronal Effected Patient age is : ' + formData.patient_age);
  }

}
