import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import { Product } from '../Product.model';
import { Router} from '@angular/router';
import { MatTableDataSource, MatTableModule } from '@angular/material/table';


@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {
  Products$? : Product[];
  displayedColumns: string[] = ['productId', 'productName', 'quantity', 'price' ];
  dataSource = new MatTableDataSource<any>();
  constructor(private dataService: DataService,private router: Router) { }

  ngOnInit() {
    this.dataService.getProducts().subscribe(data => this.dataSource.data = data); 
     
  }

  editProduct(){
    this.router.navigate(['/Product-list'])
  }

}
